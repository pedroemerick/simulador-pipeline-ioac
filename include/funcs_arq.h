/**
* @file funcs_arq.h
* @brief Declaração dos prototipos das funções que manipulam arquivos
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 02/05/2017
*/

#ifndef FUNCS_ARQ_H
#define FUNCS_ARQ_H

#include "struct.h"

#include <string>
using std::string;

#include <fstream>
using std::ifstream;

/** 
 * @brief Função para contar o numero de linhas do arquivo
 * @param arq_instrucoes Arquivo para leitura
 * @return Numero de linhas do arquivo
 */
int conta_linhas (ifstream &arq_instrucoes);

/** 
 * @brief Função que faz a leitura das instrucoes em um arquivo
 * @param arq_instrucoes Arquivo para leitura
 * @param comandos Vetor onde será armazenada as instrucoes lidas
 * @param num_instrucoes Numero de instrucoes para leitura
 */
void leitura_instru (ifstream &arq_instrucoes, Instrucoes comandos[], int num_instrucoes);

#endif