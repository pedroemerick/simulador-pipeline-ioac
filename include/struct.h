/**
* @file struct.h
* @brief Declaracao da estrutura usada no programa
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 02/05/2017
*/

#ifndef STRUCT_H
#define STRUCT_H

#include <string>
using std::string;

/**
* @struct Instrucoes instrucoes.h
* @brief Tipo estrutura que agrega os dados de cada instrução
*/
typedef struct {
    string operacao;                /**< Operação da instrução */
    string numero;                  /**< Numero se usado na instrução */
    string registradores [3];       /**< Registradores da instrução */
    int finalizada;                 /**< Número apenas para verificação se a instrução foi finalizada */
} Instrucoes;

#endif