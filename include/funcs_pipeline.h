/**
* @file funcs_pipeline.h
* @brief Declaração dos prototipos das funções que executam o pipeline
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 02/05/2017
*/

#ifndef FUNCS_PIPELINE_H
#define FUNCS_PIPELINE_H

#include "struct.h"

#include <cstdlib>
using std::atoi;

#include <iostream>
using std::cerr;
using std::endl;

/** 
 * @brief Função para juntar os dados das intruções em uma só string
 * @param comandos Vetor com os dados de cada instrução
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @return String com a instrução completa
 */
string instrucao_inteira (Instrucoes comandos[], int  ii);

/** 
 * @brief Função para fazer a mudança da ultima instrução executada
 * @param inst_chamadas Vetor com as ultimas instruções chamadas no pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 */
void muda_chamada (int inst_chamadas [], int ii);

/** 
 * @brief Função para fazer a impressão do pipeline em cada ciclo
 * @param comandos Vetor com os dados de cada instrução
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param instrucao String com a instrução em questão completa
 */
void imprimir_pipeline (Instrucoes comandos [], string pipeline [], int ii, int *ciclos, string instrucao);

/** 
 * @brief Função para fazer a organização dos estagios no pipeline correta
 * @param comandos Vetor com os dados de cada instrução
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param instrucao String com a instrução em questão completa
 */
void func_pipeline (Instrucoes comandos[], string pipeline[], int ii, int *ciclos, string instrucao);

/** 
 * @brief Função para verificar se existe a dependencia na instrução
 * @param comandos Vetor com os dados de cada instrução
 * @param num_instrucoes Numero de instrucoes
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param dependencia1 Registrador usado na instrução em questão que pode haver dependencia
 * @param dependencia2 Registrador usado na instrução em questão que pode haver dependencia
 */
void roda_ciclos (Instrucoes comandos [], int num_instrucoes, int ii, string pipeline [], int *ciclos, string dependencia1, string dependencia2);

/** 
 * @brief Função para selecionar em quais registradores pode haver dependencia na instrução
 * @param comandos Vetor com os dados de cada instrução
 * @param num_instrucoes Numero de instrucoes
 * @param ciclos Número que diz em qual ciclo está rodando
 */
void verificacao_dependencias (Instrucoes comandos [], int num_instrucoes, int *ciclos);

#endif