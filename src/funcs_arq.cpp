/**
* @file funcs_arq.cpp
* @brief Implementação das funções que manipulam arquivos
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 02/05/2017
*/

#include "funcs_arq.h"

/** 
 * @brief Função para contar o numero de linhas do arquivo
 * @param arq_instrucoes Arquivo para leitura
 * @return Numero de linhas do arquivo
 */
int conta_linhas (ifstream &arq_instrucoes)
{
    string temp;    //String temporaria apenas para melhor leitura dos dados

    // Voltando para o inicio do arquivo
    arq_instrucoes.clear();
    arq_instrucoes.seekg(0, arq_instrucoes.beg);

    int linhas = 0;

    while (getline (arq_instrucoes, temp))
        linhas ++;

    return linhas;
}

/** 
 * @brief Função que faz a leitura das instrucoes em um arquivo
 * @param arq_instrucoes Arquivo para leitura
 * @param comandos Vetor onde será armazenada as instrucoes lidas
 * @param num_instrucoes Numero de instrucoes para leitura
 */
void leitura_instru (ifstream &arq_instrucoes, Instrucoes comandos[], int num_instrucoes)
{
    string s_temp;

    arq_instrucoes.clear();
    arq_instrucoes.seekg(0, arq_instrucoes.beg);

    for (int ii = 0; ii < num_instrucoes; ii++)
    {
        getline (arq_instrucoes, s_temp, ' ');
        comandos[ii].operacao = s_temp;

        if (s_temp == "lw" || s_temp == "sw")
        {
            getline (arq_instrucoes, s_temp, ',');
            comandos[ii].registradores[0] = s_temp;
            
            getline (arq_instrucoes, s_temp, ' ');
            getline (arq_instrucoes, s_temp, '(');
            comandos[ii].numero = s_temp;

            getline (arq_instrucoes, s_temp, ')');
            comandos[ii].registradores[1] = s_temp;

            getline (arq_instrucoes, s_temp);
            comandos[ii].registradores[2] = "0";
        }
        else if (s_temp == "j")
        {
            getline (arq_instrucoes, s_temp);
            comandos[ii].registradores[0] = s_temp;
            
            comandos[ii].registradores[1] = "0";

            comandos[ii].registradores[2] = "0";
        }
        else
        {
            getline (arq_instrucoes, s_temp, ',');
            comandos[ii].registradores[0] = s_temp;
            getline (arq_instrucoes, s_temp, ' ');

            getline (arq_instrucoes, s_temp, ',');
            comandos[ii].registradores[1] = s_temp;
            getline (arq_instrucoes, s_temp, ' ');

            getline (arq_instrucoes, s_temp);
            comandos[ii].registradores[2] = s_temp;
        }
    }
}