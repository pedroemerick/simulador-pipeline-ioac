/**
* @file main.cpp
* @brief Função principal
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 02/05/2017
*/

#include <iostream>
using std::cerr;
using std::endl;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;

#include <cstdlib>
using std::atoi;

#include "funcs_arq.h"
#include "struct.h"
#include "funcs_pipeline.h"

/** 
 * @brief Função principal
 * @param argc Possui o número de argumentos com os quais a funçãoo main() foi chamada na linha de comando
 * @param argv Cada string desta matriz é um dos parâmetros da linha de comando
 */
int main (int argc, char* argv[])
{
    ifstream arq_instrucoes (argv[1]);

    if (!arq_instrucoes)
    {
        cerr << "O arquivo com as instruções não foi possível ser aberto !!!" << endl;

        return 1;
    }

    Instrucoes *comandos;

    int num_instrucoes = conta_linhas (arq_instrucoes);

    comandos = new Instrucoes [num_instrucoes];
    
    leitura_instru (arq_instrucoes, comandos, num_instrucoes);

    // Mostra instrucoes
    cerr << "Instruções lidas do arquivo " << argv[1] << ":" << endl << endl;
    for (int ii = 0; ii < num_instrucoes; ii++)
    {
        cerr << instrucao_inteira (comandos, ii) << endl;
    }
    cerr << endl;

    int ciclos = 0;

    verificacao_dependencias (comandos, num_instrucoes, &ciclos);

    cerr << "--------------------------------" << endl;
    cerr << endl;
    cerr << "Quantidade de ciclos total: " << ciclos << endl;

    return 0;
}

