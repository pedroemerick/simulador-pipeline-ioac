/**
* @file funcs_pipeline.cpp
* @brief Implementação das funções que executam o pipeline
* @author Pedro Emerick (p.emerick@live.com)
* @since 21/04/2017
* @date 03/05/2017
*/

#include "funcs_pipeline.h"

/** 
 * @brief Função para juntar os dados das intruções em uma só string
 * @param comandos Vetor com os dados de cada instrução
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @return String com a instrução completa
 */
string instrucao_inteira (Instrucoes comandos[], int  ii)
{
    string s_temp = comandos[ii].operacao;

    if (s_temp == "add" || s_temp == "sub" || s_temp == "bne" || s_temp == "beq")
    {
        s_temp += " ";
        s_temp += comandos[ii].registradores[0];
        s_temp += ", ";
        s_temp += comandos[ii].registradores[1];
        s_temp += ", ";
        s_temp += comandos[ii].registradores[2];        
    }
    else if (s_temp == "lw" || s_temp == "sw")
    {
        s_temp += " ";
        s_temp += comandos[ii].registradores[0];
        s_temp += ", ";
        s_temp += comandos[ii].numero;
        s_temp += "(";
        s_temp += comandos[ii].registradores[1];
        s_temp += ")";
    }
    else
    {
        s_temp += " ";
        s_temp += comandos[ii].registradores[0];
    }
    
    return s_temp;
} 

/** 
 * @brief Função para fazer a mudança da ultima instrução executada
 * @param inst_chamadas Vetor com as ultimas instruções chamadas no pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 */
void muda_chamada (int inst_chamadas [], int ii)
{
    inst_chamadas[2] = inst_chamadas[1];
    inst_chamadas[1] = inst_chamadas[0];
    inst_chamadas[0] = ii;
}

/** 
 * @brief Função para fazer a impressão do pipeline em cada ciclo
 * @param comandos Vetor com os dados de cada instrução
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param instrucao String com a instrução em questão completa
 */
void imprimir_pipeline (Instrucoes comandos [], string pipeline [], int ii, int *ciclos, string instrucao)
{
    *ciclos += 1;

    cerr << "--------------------------------" << endl;
	cerr << "Ciclo " << *ciclos << endl;
	cerr << "IF:\t" << pipeline[0] << endl; 
	cerr << "ID:\t" << pipeline[1] << endl; 
	cerr << "EX:\t" << pipeline[2] << endl; 
	cerr << "MEM:\t" << pipeline[3] << endl; 
	cerr << "WB:\t" << pipeline[4] << endl;

    for (int jj = 0; jj < ii; jj++)
    {
        string temp = instrucao_inteira (comandos, jj);

        if (temp == pipeline [4]) {
            comandos[jj].finalizada = 5;          
        }
    }
}

/** 
 * @brief Função para fazer a organização dos estagios no pipeline correta
 * @param comandos Vetor com os dados de cada instrução
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param instrucao String com a instrução em questão completa
 */
void func_pipeline (Instrucoes comandos[], string pipeline[], int ii, int *ciclos, string instrucao)
{
    pipeline [4] = pipeline [3];
    pipeline [3] = pipeline [2];
    pipeline [2] = pipeline [1];
    pipeline [1] = pipeline [0];
    pipeline [0] = instrucao;

    imprimir_pipeline (comandos, pipeline, ii, ciclos, instrucao);
}

/** 
 * @brief Função para verificar se existe a dependencia na instrução
 * @param comandos Vetor com os dados de cada instrução
 * @param num_instrucoes Numero de instrucoes
 * @param ii Posição em que a instrução em questão se encontra no vetor
 * @param pipeline Vetor com as instruções em cada estagio do pipeline
 * @param ciclos Número que diz em qual ciclo está rodando
 * @param dependencia1 Registrador usado na instrução em questão que pode haver dependencia
 * @param dependencia2 Registrador usado na instrução em questão que pode haver dependencia
 * @param inst_chamadas Vetor com as ultimas instruções chamadas no pipeline
 */
void roda_ciclos (Instrucoes comandos [], int num_instrucoes, int ii, string pipeline [], int *ciclos, string dependencia1, string dependencia2, int inst_chamadas [])
{
    if (ii > 0)
    {
        if (comandos[inst_chamadas[0]].operacao == "add" || comandos[inst_chamadas[0]].operacao == "sub" || comandos[inst_chamadas[0]].operacao == "lw")
        {
            if (comandos[inst_chamadas[0]].registradores[0] == dependencia1 || comandos[inst_chamadas[0]].registradores[0] == dependencia2)
            {
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
            }
        }
    }
    if (ii > 1)
    {
        if ((comandos[inst_chamadas[1]].operacao == "add" || comandos[inst_chamadas[1]].operacao == "sub" || comandos[inst_chamadas[1]].operacao == "lw") && comandos[inst_chamadas[1]].finalizada != 5)
        {
            if (comandos[inst_chamadas[1]].registradores[0] == dependencia1 || comandos[inst_chamadas[1]].registradores[0] == dependencia2)
            {
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
            }
        }
    }
    if (ii > 2)
    {
        if ((comandos[inst_chamadas[2]].operacao == "add" || comandos[inst_chamadas[2]].operacao == "sub" || comandos[inst_chamadas[2]].operacao == "lw") && comandos[inst_chamadas[2]].finalizada != 5)
        {
            if (comandos[inst_chamadas[2]].registradores[0] == dependencia1 || comandos[inst_chamadas[2]].registradores[0] == dependencia2)
            {
                func_pipeline (comandos, pipeline, ii, ciclos, "0");
            }
        }
    }

    func_pipeline (comandos, pipeline, ii, ciclos, instrucao_inteira (comandos, ii));

    muda_chamada (inst_chamadas, ii);
}

/** 
 * @brief Função para selecionar em quais registradores pode haver dependencia na instrução
 * @param comandos Vetor com os dados de cada instrução
 * @param num_instrucoes Numero de instrucoes
 * @param ciclos Número que diz em qual ciclo está rodando
 */
void verificacao_dependencias (Instrucoes comandos [], int num_instrucoes, int *ciclos)
{
    string pipeline [5] = {"0", "0", "0", "0", "0"};
    int inst_chamadas [3] = {0, 0, 0};

    for (int ii = 0; ii < num_instrucoes; ii++)
    {
        if (comandos[ii].operacao == "add" || comandos[ii].operacao == "sub")
        {
            string dependencia1 = comandos[ii].registradores[1];
            string dependencia2 = comandos[ii].registradores[2];

            roda_ciclos (comandos, num_instrucoes, ii, pipeline, ciclos, dependencia1, dependencia2, inst_chamadas);
        }
        else if (comandos[ii].operacao == "beq" || comandos[ii].operacao == "bne")
        {
            string dependencia1 = comandos[ii].registradores[0];
            string dependencia2 = comandos[ii].registradores[1];

            roda_ciclos (comandos, num_instrucoes, ii, pipeline, ciclos, dependencia1, dependencia2, inst_chamadas);

            ii = atoi (comandos[ii].registradores[2].c_str ()) - 2;
        }
        else if (comandos[ii].operacao == "lw")
        {
            string dependencia1 = comandos[ii].registradores[1];
            string dependencia2 = "0";

            roda_ciclos (comandos, num_instrucoes, ii, pipeline, ciclos, dependencia1, dependencia2, inst_chamadas);
        }
        else if (comandos[ii].operacao == "sw")
        {
            string dependencia1 = comandos[ii].registradores[0];
            string dependencia2 = comandos[ii].registradores[1];

            roda_ciclos (comandos, num_instrucoes, ii, pipeline, ciclos, dependencia1, dependencia2, inst_chamadas);  
        }
        else if (comandos[ii].operacao == "j")
        {
            string dependencia1 = "0";
            string dependencia2 = "0";

            roda_ciclos (comandos, num_instrucoes, ii, pipeline, ciclos, dependencia1, dependencia2, inst_chamadas);
            
            ii = atoi (comandos[ii].registradores[0].c_str ()) - 2;
        }

        if (ii == (num_instrucoes - 1) || ii >= num_instrucoes)
        {
            func_pipeline (comandos, pipeline, ii, ciclos, "0");
            func_pipeline (comandos, pipeline, ii, ciclos, "0");
            func_pipeline (comandos, pipeline, ii, ciclos, "0");
            func_pipeline (comandos, pipeline, ii, ciclos, "0");
        }
    }
}