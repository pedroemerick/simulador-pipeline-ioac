LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPLAGS = -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

RM = rm -rf
RM_TUDO = rm -fr

PROG = pipeline

.PHONY: init all clean debug doc doxygen

all: init $(PROG)

debug: CFLAGS += -g -O0
debug: $(PROG)

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/

$(PROG): $(OBJ_DIR)/main.o $(OBJ_DIR)/funcs_arq.o $(OBJ_DIR)/funcs_pipeline.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPLAGS) -o $(BIN_DIR)/$@ $^
	@echo "*** [Executavel $(PROG) criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR)/funcs_arq.h $(INC_DIR)/funcs_pipeline.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/funcs_arq.o: $(SRC_DIR)/funcs_arq.cpp $(INC_DIR)/funcs_arq.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

$(OBJ_DIR)/funcs_pipeline.o: $(SRC_DIR)/funcs_pipeline.cpp $(INC_DIR)/funcs_pipeline.h $(INC_DIR)/struct.h
	$(CC) -c $(CPPLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR) e $(OBJ_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
